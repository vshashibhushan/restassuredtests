package restfulbooker;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;

public class PathParameters {

	public static void main(String[] args) {
		
		 // with named parameters
//		RestAssured
//		.given()
//			.baseUri("https://restful-booker.herokuapp.com/")
//			.basePath("{basePath}/{bookingid}")
//			.pathParam("basePath", "booking")  
//			.pathParam("bookingid",2) 			
//		.when()
//			.get()
//		.then()
//			.log().all();
		
		// passing using map object ..
		/*
		 * Map<String, Object> pathParams = new HashMap<>(); pathParams.put("basePath",
		 * "booking"); pathParams.put("bookingid",2);
		 * 
		 * RestAssured .given() .baseUri("https://restful-booker.herokuapp.com/")
		 * .basePath("{basePath}/{bookingid}") .params(pathParams) .when() .get()
		 * .then() .log().all();
		 */
		
		// Inline path paraparamters ... cannot parameter the base uri using path param or params
		/*
		 * RestAssured .given() .pathParam("basePath", "booking")
		 * //.pathParam("baseurl", "restful-booker") .params("baseurl",
		 * "restful-booker") .log().all() .when()
		 * .get("https://{baseurl}.herokuapp.com/{basePath}/{bookingid}",2)
		 * .then().log().all();
		 */
		
	}
}
