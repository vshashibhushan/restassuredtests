package restfulbooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class CreateBooking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//			RequestSpecification requestSpecification = RestAssured.given();
//			
//			requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
//			requestSpecification.basePath("booking");

			String body = "{\r\n"
					+ "    \"firstname\" : \"shashi\",\r\n"
					+ "    \"lastname\" : \"virr\",\r\n"
					+ "    \"totalprice\" : 21,\r\n"
					+ "    \"depositpaid\" : true,\r\n"
					+ "    \"bookingdates\" : {\r\n"
					+ "        \"checkin\" : \"2021-12-01\",\r\n"
					+ "        \"checkout\" : \"2022-02-01\"\r\n"
					+ "    },\r\n"
					+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
					+ "}";
			
//			
//			requestSpecification.contentType(ContentType.JSON);
//			requestSpecification.log().all();
//			Response response = requestSpecification.post();
//
//			ValidatableResponse validatableResponse =response.then();
//			validatableResponse.statusCode(200);
//			validatableResponse.log().all();
//			
			// Refactoring the above code using method chaining ...BDD format
			
			RestAssured
				.given()
					.baseUri("https://restful-booker.herokuapp.com/")
					.basePath("booking").log().all()
					.contentType(ContentType.JSON)
					.body(body)
				.when()
					.post()
				.then()
					.log().all()
					.statusCode(200);
					
			
			
	}

}
