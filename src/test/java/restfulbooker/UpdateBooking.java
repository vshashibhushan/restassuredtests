package restfulbooker;

import io.restassured.RestAssured;

public class UpdateBooking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String body = "{\r\n"
				+ "    \"firstname\": \"Markkk\",\r\n"
				+ "    \"lastname\": \"Joness\",\r\n"
				+ "    \"totalprice\": 546,\r\n"
				+ "    \"depositpaid\": true,\r\n"
				+ "    \"bookingdates\": {\r\n"
				+ "        \"checkin\": \"2020-05-27\",\r\n"
				+ "        \"checkout\": \"2020-09-17\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\": \"Breakfast\"\r\n"
				+ "}";

		RestAssured
		.given()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/9")
		.body(body)
		.header("Content-Type","application/json")
		.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
		.when()
		.put()
		.then()
		.statusCode(200)
		.statusLine("HTTP/1.1 200 OK").log().all();
		
	}

}
