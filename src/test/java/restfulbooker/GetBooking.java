package restfulbooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class GetBooking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		RestAssured
		.given()
			.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("booking/{id}").pathParam("id", 9)   // place holder  - id
			//.contentType(ContentType.JSON) not mandatory for get call !!!
		.when()
			.get()
		.then()
			.log().all()
			.assertThat().statusCode(200);
		
	}

}
